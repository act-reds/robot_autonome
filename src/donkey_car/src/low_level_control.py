#!/usr/bin/python

"""
Class for low level control of our car. It assumes ros-12cpwmboard has been
installed
"""
import rospy
import RPi.GPIO as GPIO
import time

from i2cpwm_board.msg import Servo, ServoArray
from geometry_msgs.msg import Twist
from std_msgs.msg import Bool
from std_msgs.msg import Int32


import time

class ServoConvert():
    def __init__(self, id=1, init_val=0, range=4095, direction=1):
        self.value      = 0.0
        self.value_out  = init_val
        self.init_val    = init_val
        self._range     = range
        self._dir       = direction
        self.id         = id

    def get_value_out(self, value_in):
        #--- value is in [-1, 1]
        self.value      = value_in
        self.value_out  = abs(value_in)
        return(self.value_out)

class DkLowLevelCtrl():
    def __init__(self):
        GPIO.setmode (GPIO.BOARD)
        GPIO.setup (29,GPIO.OUT)
        GPIO.setup (31,GPIO.OUT)
        rospy.loginfo("Setting Up the Node...")

        rospy.init_node('dk_llc')

        self.actuators = {}
        self.actuators['left_fw']    = ServoConvert(id=4)
        self.actuators['left_bw']   = ServoConvert(id=2)
        self.actuators['right_fw']  = ServoConvert(id=3)
        self.actuators['right_bw']  = ServoConvert(id=1)
        self.step = 200
        self.max_speed = 4000
        rospy.loginfo("> Actuators corrrectly initialized")

        self._servo_msg       = ServoArray()
        for i in range(4): self._servo_msg.servos.append(Servo())

        #--- Create the servo array publisher
        self.ros_pub_servo_array    = rospy.Publisher("/servos_absolute", ServoArray, queue_size=1)
        rospy.loginfo("> Publisher corrrectly initialized")

        #--- Create the Subscriber to Twist commands
        self.ros_sub_lwheel          = rospy.Subscriber("/lwheel_desired_rate", Int32, self.set_lwheel_actuators)
        self.ros_sub_rwheel          = rospy.Subscriber("/rwheel_desired_rate", Int32, self.set_rwheel_actuators)

        rospy.loginfo("Initialization complete")
        
    ########## RWHEELS CONTROL ##########
    def set_rwheel_actuators(self, message):
        #print "right val = %d" % message.data
        new_val = message.data + self.max_speed
        prev_val = self.actuators['right_fw'].value + self.max_speed
        
        if new_val < prev_val and abs(prev_val - new_val) > self.step:
            new_val = prev_val - self.step if prev_val - self.step >= 0 else 0
        elif new_val > prev_val and abs(new_val - prev_val) > self.step:
            new_val = prev_val + self.step
            
        new_val -= self.max_speed

        if new_val < 0:
            GPIO.output(29, True)
            #print "Right : new_valmessage.data > 0:
        elif new_val > 0:
            GPIO.output(29, False)
            #print "Right : False"
            
        self.actuators['right_fw'].get_value_out(new_val)
        self.actuators['right_bw'].get_value_out(new_val)
        self.send_servo_msg()
    
    ########## LWHEELS CONTROL ##########
    def set_lwheel_actuators(self, message):
        f = open('/home/ubuntu/test_python_writing.txt','a')
        f.write('here I am')
        f.close()
        new_val = message.data + self.max_speed
        prev_val = self.actuators['left_fw'].value + self.max_speed
        
        if new_val < prev_val and abs(prev_val - new_val) > self.step:
            new_val = prev_val - self.step if prev_val - self.step >= 0 else 0
        elif new_val > prev_val and abs(new_val - prev_val) > self.step:
            new_val = prev_val + self.step

        new_val -= self.max_speed

        if new_val < 0:
            GPIO.output(31, False)
            #print "Left : False"
        elif new_val > 0:
            GPIO.output(31, True)
            #print "Left : True"
        
        
        self.actuators['left_fw'].get_value_out(new_val)
        self.actuators['left_bw'].get_value_out(new_val)
        self.send_servo_msg()
        
    def send_servo_msg(self):
        for actuator_name, servo_obj in self.actuators.iteritems():
             
            self._servo_msg.servos[servo_obj.id-1].servo = servo_obj.id
            self._servo_msg.servos[servo_obj.id-1].value = servo_obj.value_out
            #rospy.loginfo("Sending to %s command %d"%(actuator_name, servo_obj.value_out))

        self.ros_pub_servo_array.publish(self._servo_msg)

    def run(self):

        #--- Set the control rate
        rate = rospy.Rate(5)

        while not rospy.is_shutdown():
            rate.sleep()
            
        GPIO.cleanup()

if __name__ == "__main__":
    dk_llc     = DkLowLevelCtrl()
    dk_llc.run()
